# ABB_QP_Controller



## Getting started

```
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/ter_stages/abb_qp_controller.git
cd abb_qp_controller
git submodule sync
git submodule update --init
git submodule update --recursive --remote
```
### Useful Links

- [abb_robot_driver](https://github.com/ros-industrial/abb_robot_driver)
- [YuMi bringup example](https://github.com/ros-industrial/abb_robot_driver/tree/master/abb_robot_bringup_examples#example-3-ex3_rws_and_egm_yumi_robot)
- [ROS Control](http://wiki.ros.org/ros_control)
- [velocity_qp](https://gitlab.inria.fr/auctus-team/components/control/velocity_qp)
- [ur_qp_control](https://gitlab.inria.fr/auctus-team/components/robots/ur/ur_qp_control)

### Project Entry Point

[UR Launch File](https://gitlab.inria.fr/auctus-team/components/robots/ur/ur_qp_control/-/blob/master/launch/run.launch)
